package cz.kruzsoftware.kraus.stonebird;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class EditTransactionActivity extends AppCompatActivity {

    EditTransactionActivity context = this;

    FirebaseUser user;

    ArrayList<String> categories;

    Transaction transaction;
    double originalAmount = 0;
    String transactionId;
    String currency, selectedCurrency;
    ArrayList<String> currencies;
    JSONObject rates;

    EditText amountEditText, labelEditText;
    RadioGroup radioGroup;
    RadioButton expenseRadio, incomeRadio;
    ProgressBar progressBar, mainProgressBar;
    FloatingActionButton fab;
    boolean fabClickable = true;
    ScrollView scrollView;
    Button currencyButton, dateButton, categoryButton;
    TextView actualAmountTextView;

    DatabaseReference transactionsRef, profileReference;

    SharedPreferences defSharedPref;

    RequestQueue requestQueue;

    SimpleDateFormat simpleDateFormat;

    LayoutInflater layoutInflater;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_transaction);

        layoutInflater =  (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        if(layoutInflater == null) finish();

        requestQueue = Volley.newRequestQueue(this);

        new Transaction();

        user = FirebaseAuth.getInstance().getCurrentUser();

        currencies = new ArrayList<>();

        defSharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        amountEditText = findViewById(R.id.editText_amount);
        labelEditText = findViewById(R.id.editText_label);
        radioGroup = findViewById(R.id.radioGroup_transactionType);
        expenseRadio = findViewById(R.id.radioButton_expense);
        incomeRadio = findViewById(R.id.radioButton_income);
        fab = findViewById(R.id.fab_edit);
        progressBar = findViewById(R.id.progressBar);
        mainProgressBar = findViewById(R.id.progressBar_main);
        scrollView = findViewById(R.id.scrollView);
        currencyButton = findViewById(R.id.button_currency);
        dateButton = findViewById(R.id.button_date);
        categoryButton = findViewById(R.id.button_category);
        actualAmountTextView = findViewById(R.id.textView_actualAmount);

        categoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                        if(categories.size() > 0) {
                            categoriesAD().show();
                        } else {
                            newCategoryAD().show();
                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        simpleDateFormat = new SimpleDateFormat("EEE d MMM yyyy", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getDefault());

        Intent intent = getIntent();
        transactionId = intent.getStringExtra("id");

        FirebaseDatabase.getInstance().getReference("currencies").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    currencies.add(child.getValue(String.class));
                }
                updateUi();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                finish();
            }
        });

        profileReference = FirebaseDatabase.getInstance()
                .getReference("profiles")
                .child(user.getUid())
                .child(defSharedPref.getString(getString(R.string.shared_pref_profile_id) + user.getUid(), ""));

        profileReference.child("currency").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currency = dataSnapshot.getValue(String.class);
                selectedCurrency = currency;

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.POST,
                        "http://www.floatrates.com/daily/" + currency + ".json",
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                rates = response;
                                updateUi();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                finish();
                            }
                        });
                requestQueue.add(jsonObjectRequest);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                finish();
            }
        });
        profileReference.child("categories").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                categories = new ArrayList<>();
                for(DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    categories.add(dataSnapshot1.getValue(String.class));
                }

                updateUi();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        transactionsRef = profileReference.child("transactions").child(transactionId);
        transactionsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                transaction = dataSnapshot.getValue(Transaction.class);
                if(transaction == null) transaction = new Transaction(0, new Date(), "", "");
                originalAmount = transaction.amount;
                selectedCurrency = currency;
                updateUi();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                finish();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fabClickable) return;

                transaction.label = labelEditText.getText().toString();
                rethinkActualValue();

                transaction.category = categoryButton.getText().toString().trim();

                if(transaction.category.equals("")) {
                    if(categories.size() > 0) {
                        categoriesAD().show();
                    } else {
                        newCategoryAD().show();
                    }
                    return;
                }

                fabClickable = false;
                progressBar.setVisibility(View.VISIBLE);
                transactionsRef.setValue(transaction, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            progressBar.setVisibility(View.GONE);
                            fabClickable = true;
                            transaction.amount = Math.abs(transaction.amount);
                        } else {
                            finish();
                        }
                    }
                });
            }
        });

        updateUi();
    }

    @SuppressLint("SetTextI18n")
    void updateUi() {
        if(transaction == null || rates == null || currencies.size() == 0 || categories == null) {
            scrollView.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            mainProgressBar.setVisibility(View.VISIBLE);
        } else {
            scrollView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
            mainProgressBar.setVisibility(View.GONE);

            if(transaction.label != null) {
                if(transaction.amount < 0) {
                    expenseRadio.setChecked(true);
                    incomeRadio.setChecked(false);
                } else {
                    expenseRadio.setChecked(false);
                    incomeRadio.setChecked(true);
                }

                NumberFormat numberFormat = NumberFormat.getInstance();
                numberFormat.setGroupingUsed(false);

                amountEditText.setText(numberFormat.format(Math.abs(originalAmount)).replace(',', '.'));
                labelEditText.setText(transaction.label);
                categoryButton.setText(transaction.category);
            }

            if(transaction.category != null && transaction.category.equals("") && categories.size() > 0) {
                categoryButton.setText(categories.get(0));
            } else {
                categoryButton.setText(transaction.category);
            }

            rethinkActualValue();
            currencyButton.setText(selectedCurrency);

            currencyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String[] array = currencies.toArray(new String[0]);
                    for(int i = 0; i < array.length; i++) {
                        array[i] = array[i].toUpperCase();
                    }
                    new AlertDialog.Builder(context)
                            .setItems(array, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    selectedCurrency = currencies.get(which);
                                    currencyButton.setText(selectedCurrency);
                                    rethinkActualValue();
                                }
                            })
                            .setNegativeButton(R.string.cancel, null)
                            .show();

                }
            });

            try {
                Date date = Transaction.universalSdf.parse(transaction.date);
                final Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                calendar.setTime(date);
                dateButton.setText(simpleDateFormat.format(date));
                dateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DatePickerDialog(
                                context,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                        calendar.set(year, month, dayOfMonth);
                                        Date date = calendar.getTime();
                                        transaction.date = Transaction.universalSdf.format(date);
                                        updateUi();
                                    }
                                },
                                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
                        ).show();
                    }
                });
            } catch (ParseException e) {
                e.printStackTrace();
            }

            amountEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    rethinkActualValue();
                }
            });
        }
    }

    private AlertDialog newCategoryAD() {
        final View view = layoutInflater.inflate(R.layout.alertdialog_new_category, null);
        return new AlertDialog.Builder(context)
                .setTitle(R.string.add_new_category)
                .setView(view)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String string = ((EditText) view.findViewById(R.id.editText)).getText().toString();
                        if(string.equals("") || categories.contains(string)) return;

                        categories.add(string);
                        profileReference.child("categories").setValue(categories);
                        transaction.category = string;
                        updateUi();
                    }
                }).create();
    }

    private AlertDialog categoriesAD() {
        AlertDialog.Builder categoriesBuilder = new AlertDialog.Builder(context);
        final ArrayList<String> arrayList = new ArrayList<>(categories);
        arrayList.add(getString(R.string.add_new_category));
        return categoriesBuilder.setItems(arrayList.toArray(new String[0]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == arrayList.size() - 1) {
                    newCategoryAD().show();
                } else {
                    transaction.category = arrayList.get(which);
                }
                updateUi();
            }
        })
                .setNegativeButton(R.string.cancel, null).create();
    }

    void rethinkActualValue() {
        String doubleString = amountEditText.getText().toString();
        if (doubleString.equals("")) doubleString = "0";
        transaction.amount = Double.parseDouble(doubleString);
        if(expenseRadio.isChecked()) transaction.amount *= -1;
        originalAmount = transaction.amount;
        if(!currency.equals(selectedCurrency)) {
            try {
                transaction.amount *= rates.getJSONObject(selectedCurrency).getDouble("inverseRate");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        NumberFormat numberFormat = NumberFormat.getInstance(Locale.ENGLISH);
        numberFormat.setMaximumFractionDigits(2);

        String string = "= " + numberFormat.format(Math.abs(transaction.amount)) + " " + currency.toUpperCase();
        actualAmountTextView.setText(string);
    }
}
