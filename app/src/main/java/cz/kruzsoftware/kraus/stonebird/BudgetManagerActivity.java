package cz.kruzsoftware.kraus.stonebird;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

public class BudgetManagerActivity extends AppCompatActivity {

    SharedPreferences defSharedPreferences;

    private ProgressBar progressBar;
    private View scrollView;
    private LinearLayout linearLayout, detailsLinearLayout;
    private FloatingActionButton fab;
    private TextView toolbarProfileNameTextView, toolbarMoneyTextView;
    private TextView incomeTextView, expenseTextView, totalTextView;
    private ColorStateList twSmallColorStateList, twMediumColorStateList;
    private Button buttonDate, buttonRange;
    private BottomNavigationView bottomNavigationView;

    private FirebaseUser user;

    private FirebaseDatabase db;
    private DatabaseReference profilesRef;

    private Profile profile;
    private ArrayList<Transaction> transactionsOrdered;

    AppCompatActivity context = this;

    View selectedTransaction;

    Date dateBegin, dateEnd, dateSelected;
    SimpleDateFormat daySdf, weekSdf, monthSdf, yearSdf;
    private static final int RANGE_DAY = 780;
    private static final int RANGE_WEEK = 256;
    private static final int RANGE_MONTH = 590;
    private static final int RANGE_YEAR = 761;
    private static final int RANGE_ALL = 901;
    private int range = RANGE_WEEK;

    private NumberFormat moneyNumberFormat;
    private LayoutInflater layoutInflater;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            updateUi(true, item.getItemId());
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_manager);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }
        Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(getResources().getColor(R.color.toolbarItemColor), PorterDuff.Mode.SRC_ATOP);

        scrollView = findViewById(R.id.scrollView);
        progressBar = findViewById(R.id.progressBar_budgMan);
        linearLayout = findViewById(R.id.linearLayout);
        detailsLinearLayout = findViewById(R.id.linearLayout_details);
        fab = findViewById(R.id.fab_add);
        toolbarMoneyTextView = findViewById(R.id.textView_toolbarProfileMoney);
        toolbarProfileNameTextView = findViewById(R.id.textView_toolbarProfileName);
        buttonDate = findViewById(R.id.button_date);
        buttonRange = findViewById(R.id.button_range);
        incomeTextView = findViewById(R.id.textView_income);
        expenseTextView = findViewById(R.id.textView_expense);
        totalTextView = findViewById(R.id.textView_total);

        twSmallColorStateList = totalTextView.getTextColors();
        twMediumColorStateList = buttonDate.getTextColors();

        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        dateSelected = new Date();
        calculateRange();

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                calendar.setTime(dateSelected);
                new DatePickerDialog(
                        context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Calendar calendar1 = Calendar.getInstance(TimeZone.getDefault());
                                calendar1.set(year, month, dayOfMonth);
                                dateSelected = calendar1.getTime();

                                calculateRange();
                                updateUi(false, bottomNavigationView.getSelectedItemId());
                            }
                        },
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
                ).show();
            }
        });
        buttonRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] ranges = {
                        context.getString(R.string.day),
                        context.getString(R.string.week),
                        context.getString(R.string.month),
                        context.getString(R.string.year),
                        getString(R.string.all)
                };
                new AlertDialog.Builder(context)
                        .setItems(ranges, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        range = RANGE_DAY;
                                        break;

                                    case 1:
                                        range = RANGE_WEEK;
                                        break;

                                    case 2:
                                        range = RANGE_MONTH;
                                        break;

                                    case 3:
                                        range = RANGE_YEAR;
                                        break;

                                    case 4:
                                        range = RANGE_ALL;
                                }

                                calculateRange();
                                updateUi(false, bottomNavigationView.getSelectedItemId());
                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .show();
            }
        });

        daySdf = new SimpleDateFormat("EEE d MMM yyyy", Locale.ENGLISH);
        daySdf.setTimeZone(TimeZone.getDefault());
        weekSdf = new SimpleDateFormat("d MMM yyyy", Locale.ENGLISH);
        weekSdf.setTimeZone(TimeZone.getDefault());
        monthSdf = new SimpleDateFormat("MMM yyyy", Locale.ENGLISH);
        monthSdf.setTimeZone(TimeZone.getDefault());
        yearSdf = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        yearSdf.setTimeZone(TimeZone.getDefault());

        moneyNumberFormat = NumberFormat.getInstance(Locale.ENGLISH);
        moneyNumberFormat.setMaximumFractionDigits(2);

        layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

        user = FirebaseAuth.getInstance().getCurrentUser();

        defSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        db = FirebaseDatabase.getInstance();
        profilesRef = db.getReference("profiles").child(user.getUid());

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditTransactionActivity.class);
                intent.putExtra("id", profile.transactions == null ? "0" : Integer.toString(profile.transactions.size()));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.budget_manager_menu, menu);
        menu.findItem(R.id.menuItem_profiles).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(context, ProfileActivity.class);
                startActivity(intent);
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        profilesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                // Iterate through all profiles
                int i = 0;
                String preferenceValue = defSharedPreferences.getString(getString(R.string.shared_pref_profile_id) + user.getUid(), "null");
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    i++;

                    // Check if profile equals the one selected in shared pref
                    if(preferenceValue.equals(child.getKey()) || preferenceValue.equals("null")) {
                        try {
                            profile = child.getValue(Profile.class);
                        } catch (DatabaseException e) {
                            e.printStackTrace();

                        }
                        if(profile == null) profile = new Profile("Unknown Profile", "usd");
                        updateUi(false, bottomNavigationView.getSelectedItemId());
                        if(preferenceValue.equals("null")) {
                            defSharedPreferences.edit().putString(getString(R.string.shared_pref_profile_id) + user.getUid(), "0").apply();
                        }
                        break;
                    }
                }

                // There are no children
                if (i == 0) {
                    profile = new Profile("My Profile", "usd");
                    profilesRef.child("0").setValue(profile);
                    defSharedPreferences.edit().putString(getString(R.string.shared_pref_profile_id) + user.getUid(), "0").apply();
                    updateUi(false, bottomNavigationView.getSelectedItemId());
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        updateUi(false, bottomNavigationView.getSelectedItemId());
    }

    private void updateUi(boolean removeViews, int menuItemId) {
        if(profile != null) {
            profile.clean();
            profilesRef.child(defSharedPreferences.getString(getString(R.string.shared_pref_profile_id) + user.getUid(), "null")).child("categories").setValue(profile.categories);

            setTitle("");
            toolbarProfileNameTextView.setText(profile.name);
            String money = moneyNumberFormat.format(profile.value()) + " " + profile.currency.toUpperCase();
            toolbarMoneyTextView.setText(money);
            progressBar.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
            if (layoutInflater != null && profile.transactions != null) {
                if (removeViews) linearLayout.removeAllViews();

                calculateRange();
                updateUiMainCard();

                switch (menuItemId) {
                    case R.id.navigation_list:
                        updateUiList();
                        break;

                    case R.id.navigation_statistics:
                        updateUiStats();
                        break;
                }
            }
        } else {
            progressBar.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }
    }

    private void updateUiMainCard() {
        switch (range) {
            case RANGE_DAY:
                buttonRange.setText(R.string.day);
                buttonDate.setText(daySdf.format(dateBegin));
                break;

            case RANGE_WEEK:
                buttonRange.setText(R.string.week);
                String string = weekSdf.format(dateBegin) + " -\n" + weekSdf.format(dateEnd);
                buttonDate.setText(string);
                break;

            case RANGE_MONTH:
                buttonRange.setText(R.string.month);
                buttonDate.setText(monthSdf.format(dateBegin));
                break;

            case RANGE_YEAR:
                buttonRange.setText(R.string.year);
                buttonDate.setText(yearSdf.format(dateBegin));
                break;

            case RANGE_ALL:
                buttonRange.setText(R.string.all);
                String string2 = weekSdf.format(dateBegin) + " -\n" + weekSdf.format(dateEnd);
                buttonDate.setText(string2);
                break;
        }

        transactionsOrdered = new ArrayList<>(profile.transactions);
        transactionsOrdered.remove(0);
        for (int i = 0; i < transactionsOrdered.size(); i++) {
            for (int j = 0; j < transactionsOrdered.size() - 1; j++) {
                Transaction currentTransaction = transactionsOrdered.get(j);
                if (currentTransaction == null || currentTransaction.date.equals(""))
                    continue;
                int index = j;
                Transaction nextTransaction;
                do {
                    nextTransaction = transactionsOrdered.get(++index);
                }
                while ((nextTransaction == null || nextTransaction.date.equals("")) && index < transactionsOrdered.size() - 1);
                if (nextTransaction == null) continue;

                try {
                    if (Transaction.universalSdf.parse(currentTransaction.date).before(Transaction.universalSdf.parse(nextTransaction.date))) {
                        transactionsOrdered.set(index, currentTransaction);
                        transactionsOrdered.set(j, nextTransaction);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        double income = 0, expense = 0, total = 0;
        for (int i = transactionsOrdered.size() - 1; i >= 0; i--) {
            Transaction transaction = transactionsOrdered.get(i);
            try {
                if (transaction == null || Transaction.universalSdf.parse(transaction.date).after(dateEnd) || Transaction.universalSdf.parse(transaction.date).before(dateBegin)) {
                    transactionsOrdered.remove(i);
                    continue;
                }
            } catch (ParseException e) {
                e.printStackTrace();
                transactionsOrdered.remove(i);
                continue;
            }

            total += transaction.amount;
            if (transaction.amount < 0) expense += Math.abs(transaction.amount);
            if (transaction.amount > 0) income += transaction.amount;
        }

        String incomeString = moneyNumberFormat.format(income) + " " + profile.currency.toUpperCase();
        String expenseString = moneyNumberFormat.format(expense) + " " + profile.currency.toUpperCase();
        String totalString = (total > 0 ? "+" : "") + moneyNumberFormat.format(total) + " " + profile.currency.toUpperCase();
        incomeTextView.setText(incomeString);
        incomeTextView.setTextColor(income > 0 ? getResources().getColor(android.R.color.holo_green_dark) : twSmallColorStateList.getDefaultColor());
        expenseTextView.setText(expenseString);
        expenseTextView.setTextColor(expense > 0 ? getResources().getColor(android.R.color.holo_red_dark) : twSmallColorStateList.getDefaultColor());
        totalTextView.setText(totalString);
        if (total > 0)
            totalTextView.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
        else if (total < 0)
            totalTextView.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        else totalTextView.setTextColor(twSmallColorStateList.getDefaultColor());
    }

    private void updateUiList() {
        detailsLinearLayout.removeAllViews();
        ArrayList<View> checkedViews = new ArrayList<>();

        for (int i = 1; i < profile.transactions.size(); i++) {
            final Transaction transaction = profile.transactions.get(i);
            View view = linearLayout.findViewWithTag(i);
            if (view == null) {
                view = layoutInflater.inflate(R.layout.transaction_layout, null);
                linearLayout.addView(view);
            }
            view.setTag(i);

            checkedViews.add(view);

            try {
                if (transaction == null || ((Transaction.universalSdf.parse(transaction.date).after(dateEnd) || Transaction.universalSdf.parse(transaction.date).before(dateBegin)) && range != RANGE_ALL)) {
                    linearLayout.removeView(view);
                    continue;
                }
            } catch (ParseException e) {
                e.printStackTrace();
                linearLayout.removeView(view);
                continue;
            }

            ((TextView) view.findViewById(R.id.textView_label)).setText(transaction.label);

            TextView moneyTextView = view.findViewById(R.id.textView_money);
            String string = (transaction.amount > 0 ? "+" : "") + moneyNumberFormat.format(transaction.amount) + " " + profile.currency.toUpperCase();
            moneyTextView.setText(string);
            if (transaction.amount < 0)
                moneyTextView.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            else if (transaction.amount > 0)
                moneyTextView.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            else
                moneyTextView.setTextColor(twMediumColorStateList.getDefaultColor());

            ((TextView) view.findViewById(R.id.textView_date)).setText(daySdf.format(Transaction.parseDate(transaction.date)));
            ((TextView) view.findViewById(R.id.textView_category)).setText(transaction.category);

            final int finalI = i;
            view.findViewById(R.id.button_edit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EditTransactionActivity.class);
                    intent.putExtra("id", Integer.toString(finalI));
                    startActivity(intent);
                }
            });

            view.findViewById(R.id.button_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(context)
                            .setMessage(getString(R.string.delete_transaction_message) + " " + transaction.label + "?")
                            .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Transaction tr = new Transaction();
                                    tr.date = "";
                                    fab.setVisibility(View.GONE);
                                    scrollView.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.VISIBLE);
                                    profilesRef.child(defSharedPreferences.getString(getString(R.string.shared_pref_profile_id) + user.getUid(), ""))
                                            .child("transactions").child(Integer.toString(finalI)).setValue(tr, new DatabaseReference.CompletionListener() {
                                        @Override
                                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                            updateUi(false, bottomNavigationView.getSelectedItemId());
                                        }
                                    });
                                }
                            })
                            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.equals(selectedTransaction)) {
                        v.findViewById(R.id.linearLayout).setVisibility(View.GONE);
                        selectedTransaction = null;
                    } else {
                        if (selectedTransaction != null)
                            selectedTransaction.findViewById(R.id.linearLayout).setVisibility(View.GONE);
                        v.findViewById(R.id.linearLayout).setVisibility(View.VISIBLE);
                        selectedTransaction = v;
                    }
                }
            });
        }

        for (int i = linearLayout.getChildCount() - 1; i >= 0; i--) {
            View view = linearLayout.getChildAt(i);
            if (!checkedViews.contains(view)) linearLayout.removeView(view);
        }

        for (int i = 0; i < transactionsOrdered.size(); i++) {
            Transaction transaction = transactionsOrdered.get(i);
            View view = linearLayout.findViewWithTag(profile.transactions.indexOf(transaction));
            if (linearLayout.indexOfChild(view) != i) {
                linearLayout.removeView(view);
                if (view != null)
                    linearLayout.addView(view, i);
            }
        }
    }

    private void updateUiStats() {
        HashMap<String, ArrayList<Transaction>> categoriesHashMap = new HashMap<>();

        int count = 0;
        for(int i = 1; i < profile.transactions.size(); i++) {
            Transaction transaction = profile.transactions.get(i);
            if (transaction == null || transaction.label == null || transaction.label.trim().equals("")) continue;
            try {
                Date date = Transaction.universalSdf.parse(transaction.date);
                if (!date.before(dateBegin) && !date.after(dateEnd)) {
                    count++;

                    if (!categoriesHashMap.containsKey(transaction.category))
                        categoriesHashMap.put(transaction.category, new ArrayList<Transaction>());
                    categoriesHashMap.get(transaction.category).add(transaction);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        /*
         * DETAIL STATS
         */
        ArrayList<View> detailsViewsChecked = new ArrayList<>();
        for (Map.Entry<String, ArrayList<Transaction>> entry : categoriesHashMap.entrySet()) {
            View view = detailsLinearLayout.findViewWithTag(entry.getKey());
            if (view == null) {
                view = layoutInflater.inflate(R.layout.category_details_layout, null);
                view.setTag(entry.getKey());
                detailsLinearLayout.addView(view);
            }

            double income = 0, expense = 0, total = 0;
            for(Transaction transaction : entry.getValue()) {
                total += transaction.amount;
                if(transaction.amount < 0) expense += Math.abs(transaction.amount);
                if(transaction.amount > 0) income += transaction.amount;
            }

            TextView textViewLabel = view.findViewById(R.id.textView_categoryName);
            TextView textViewIncome = view.findViewById(R.id.textView_income);
            TextView textViewExpense = view.findViewById(R.id.textView_expense);
            TextView textViewTotal = view.findViewById(R.id.textView_total);

            textViewLabel.setText(entry.getKey());

            String stringIncome = moneyNumberFormat.format(income) + " " + profile.currency.toUpperCase();
            textViewIncome.setText(stringIncome);
            textViewIncome.setTextColor(income > 0 ? getResources().getColor(android.R.color.holo_green_dark) : twSmallColorStateList.getDefaultColor());

            String stringExpense = moneyNumberFormat.format(expense) + " " + profile.currency.toUpperCase();
            textViewExpense.setText(stringExpense);
            textViewExpense.setTextColor(expense > 0 ? getResources().getColor(android.R.color.holo_red_dark) : twSmallColorStateList.getDefaultColor());

            String stringTotal = (total > 0 ? "+" : "") + moneyNumberFormat.format(total) + " " + profile.currency.toUpperCase();
            textViewTotal.setText(stringTotal);
            if(total > 0) textViewTotal.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            else if(total < 0) textViewTotal.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            else textViewTotal.setTextColor(twSmallColorStateList.getDefaultColor());

            detailsViewsChecked.add(view);
        }

        for(int i = detailsLinearLayout.getChildCount() - 1; i >= 0; i--) {
            View view = detailsLinearLayout.getChildAt(i);
            if(!detailsViewsChecked.contains(view)) detailsLinearLayout.removeView(view);
        }



        /*
         * LINE GRAPHS
         */

        View totalGraphView = linearLayout.findViewWithTag("totalGraph");
        View incomeExpenseGraphView = linearLayout.findViewWithTag("incomeExpenseGraph");


        if(range != RANGE_DAY && count > 1) {
            if (totalGraphView == null) {
                totalGraphView = layoutInflater.inflate(R.layout.line_graph_layout, null);
                totalGraphView.setTag("totalGraph");
                linearLayout.addView(totalGraphView);
            }
            if(incomeExpenseGraphView == null) {
                incomeExpenseGraphView = layoutInflater.inflate(R.layout.line_graph_layout, null);
                incomeExpenseGraphView.setTag("incomeExpenseGraph");
                linearLayout.addView(incomeExpenseGraphView);
            }

            GraphView totalGraph = totalGraphView.findViewById(R.id.graph);
            StaticLabelsFormatter staticLabelsFormatterTotal = new StaticLabelsFormatter(totalGraph);
            totalGraph.setTitle(getString(R.string.total));

            GraphView incomeExpenseGraph = incomeExpenseGraphView.findViewById(R.id.graph);
            StaticLabelsFormatter staticLabelsFormatterIncomeExpense = new StaticLabelsFormatter(incomeExpenseGraph);
            String string = getString(R.string.income) + "/" + getString(R.string.expense);
            incomeExpenseGraph.setTitle(string);

            Double days = Math.ceil(getTimeDifference(dateEnd, dateBegin) / 86400000);
            DataPoint[] dataPointsTotal = new DataPoint[days.intValue() + 1];
            DataPoint[] dataPointsIncome = new DataPoint[days.intValue() + 1];
            DataPoint[] dataPointsExpense = new DataPoint[days.intValue() + 1];
            String[] horizontalLabels = new String[days.intValue() + 1];

            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            calendar.setTime(dateBegin);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);

            for (int i = 0; i <= days; i++) {
                double total = 0, income = 0, expense = 0;
                Date date = calendar.getTime();

                for (Transaction transaction : profile.transactions) {
                    if (transaction == null || transaction.label == null || transaction.label.trim().equals(""))
                        continue;
                    try {
                        Date transactionDate = Transaction.universalSdf.parse(transaction.date);

                        if (!transactionDate.after(date)) total += transaction.amount;
                        if (sameDate(transactionDate, date)) {
                            if(transaction.amount < 0) expense += Math.abs(transaction.amount);
                            if(transaction.amount > 0) income += transaction.amount;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                dataPointsTotal[i] = new DataPoint(i, total);
                dataPointsExpense[i] = new DataPoint(i, expense);
                dataPointsIncome[i] = new DataPoint(i, income);
                horizontalLabels[i] = "";

                calendar.add(Calendar.DATE, 1);
            }

            staticLabelsFormatterTotal.setHorizontalLabels(horizontalLabels);
            totalGraph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatterTotal);
            staticLabelsFormatterIncomeExpense.setHorizontalLabels(horizontalLabels);
            incomeExpenseGraph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatterIncomeExpense);

            totalGraph.getViewport().setXAxisBoundsManual(true);
            totalGraph.getViewport().setMaxX(days);
            incomeExpenseGraph.getViewport().setXAxisBoundsManual(true);
            incomeExpenseGraph.getViewport().setMaxX(days);

            LineGraphSeries<DataPoint> lineGraphSeriesTotal = new LineGraphSeries<>(dataPointsTotal);
            lineGraphSeriesTotal.setColor(getResources().getColor(android.R.color.black));
            lineGraphSeriesTotal.setTitle(getString(R.string.total));
            totalGraph.removeAllSeries();
            totalGraph.addSeries(lineGraphSeriesTotal);

            LineGraphSeries<DataPoint> lineGraphSeriesIncome = new LineGraphSeries<>(dataPointsIncome);
            lineGraphSeriesIncome.setColor(getResources().getColor(android.R.color.holo_green_dark));
            lineGraphSeriesIncome.setTitle(getString(R.string.income));
            LineGraphSeries<DataPoint> lineGraphSeriesExpense = new LineGraphSeries<>(dataPointsExpense);
            lineGraphSeriesExpense.setColor(getResources().getColor(android.R.color.holo_red_dark));
            lineGraphSeriesExpense.setTitle(getString(R.string.expense));
            incomeExpenseGraph.removeAllSeries();
            incomeExpenseGraph.addSeries(lineGraphSeriesExpense);
            incomeExpenseGraph.addSeries(lineGraphSeriesIncome);
        } else {
            if(totalGraphView != null) linearLayout.removeView(totalGraphView);
            if(incomeExpenseGraphView != null) linearLayout.removeView(incomeExpenseGraphView);
        }
    }

    private long getTimeDifference(@NonNull Date time1, @NonNull Date time2) {
        return Math.abs(time1.getTime() - time2.getTime());
    }

    private boolean sameDate(@NonNull Date date1, @NonNull Date date2) {
        Calendar calendar1 = Calendar.getInstance(TimeZone.getDefault());
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance(TimeZone.getDefault());
        calendar2.setTime(date2);
        return calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
                && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) && calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH);
    }

    private void calculateRange() {
        Calendar selectedCalendar = Calendar.getInstance(TimeZone.getDefault());
        selectedCalendar.setTime(dateSelected);
        switch (range) {
            case RANGE_DAY:
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                selectedCalendar.set(Calendar.MINUTE, 0);
                selectedCalendar.set(Calendar.SECOND, 0);
                dateBegin = selectedCalendar.getTime();
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 23);
                selectedCalendar.set(Calendar.MINUTE, 59);
                selectedCalendar.set(Calendar.SECOND, 59);
                dateEnd = selectedCalendar.getTime();
                break;

            case RANGE_WEEK:
                while(selectedCalendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
                    selectedCalendar.add(Calendar.DATE, -1);
                }
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                selectedCalendar.set(Calendar.MINUTE, 0);
                selectedCalendar.set(Calendar.SECOND, 0);
                dateBegin = selectedCalendar.getTime();
                while(selectedCalendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                    selectedCalendar.add(Calendar.DATE, 1);
                }
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 23);
                selectedCalendar.set(Calendar.MINUTE, 59);
                selectedCalendar.set(Calendar.SECOND, 59);
                dateEnd = selectedCalendar.getTime();
                break;

            case RANGE_MONTH:
                while(selectedCalendar.get(Calendar.DAY_OF_MONTH) != 1) {
                    selectedCalendar.add(Calendar.DATE, -1);
                }
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                selectedCalendar.set(Calendar.MINUTE, 0);
                selectedCalendar.set(Calendar.SECOND, 0);
                dateBegin = selectedCalendar.getTime();
                do  {
                    selectedCalendar.add(Calendar.DATE, 1);
                } while(selectedCalendar.get(Calendar.DAY_OF_MONTH) != 1);
                selectedCalendar.add(Calendar.DATE, -1);
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 23);
                selectedCalendar.set(Calendar.MINUTE, 59);
                selectedCalendar.set(Calendar.SECOND, 59);
                dateEnd = selectedCalendar.getTime();
                break;

            case RANGE_YEAR:
                while(selectedCalendar.get(Calendar.DAY_OF_YEAR) != 1) {
                    selectedCalendar.add(Calendar.DATE, -1);
                }
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                selectedCalendar.set(Calendar.MINUTE, 0);
                selectedCalendar.set(Calendar.SECOND, 0);
                dateBegin = selectedCalendar.getTime();
                do  {
                    selectedCalendar.add(Calendar.DATE, 1);
                } while(selectedCalendar.get(Calendar.DAY_OF_YEAR) != 1);
                selectedCalendar.add(Calendar.DATE, -1);
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 23);
                selectedCalendar.set(Calendar.MINUTE, 59);
                selectedCalendar.set(Calendar.SECOND, 59);
                dateEnd = selectedCalendar.getTime();
                break;

            case RANGE_ALL:
                dateBegin = null;
                dateEnd = null;
                for(int i = 1; i < profile.transactions.size(); i++) {
                    Transaction transaction = profile.transactions.get(i);
                    if(transaction == null || transaction.label == null || transaction.label.equals("")) continue;

                    try {
                        Date date = Transaction.universalSdf.parse(transaction.date);
                        if(dateBegin == null || date.before(dateBegin)) dateBegin = date;
                        if(dateEnd == null || date.after(dateEnd)) dateEnd = date;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if(dateBegin == null) dateBegin = new Date();
                if(dateEnd == null) dateEnd = new Date();

                selectedCalendar.setTime(dateBegin);
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 0);
                selectedCalendar.set(Calendar.MINUTE, 0);
                selectedCalendar.set(Calendar.SECOND, 0);
                dateBegin = selectedCalendar.getTime();
                selectedCalendar.setTime(dateEnd);
                selectedCalendar.set(Calendar.HOUR_OF_DAY, 23);
                selectedCalendar.set(Calendar.MINUTE, 59);
                selectedCalendar.set(Calendar.SECOND, 59);
                dateEnd = selectedCalendar.getTime();
                break;
        }
    }
}
