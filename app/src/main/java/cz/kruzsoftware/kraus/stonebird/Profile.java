package cz.kruzsoftware.kraus.stonebird;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Profile {

    public String name;
    public String currency;

    public ArrayList<Transaction> transactions;
    public ArrayList<String> categories;

    public Profile() {
        // Needed for realtime db
    }

    Profile(String name, String currency) {
        this.name = name;
        this.currency = currency;
    }

    public double value() {
        if(transactions == null) return 0;
        double result = 0;
        for(int i = 1; i < transactions.size(); i++) {
            Transaction transaction = transactions.get(i);
            if(transaction == null) continue;
            result += transaction.amount;
        }
        return result;
    }

    public int clean() {
        if(categories == null) categories = new ArrayList<>();
        int total = 0;
        for(int i = categories.size() - 1; i >= 0; i--) {
            String string = categories.get(i);
            int count = 0;
            for(Transaction transaction : transactions) {
                if(transaction == null || transaction.category == null) continue;
                if(transaction.category.equals(string) && !(transaction.category.equals("d") && transaction.label.equals("d") && transaction.amount == 1)) {
                    count++;
                }
            }
            if(count == 0 || string.trim().equals("")) {
                categories.remove(i);
                total++;
            }
        }

        for(int i = 1; i < transactions.size(); i++) {
            Transaction transaction = transactions.get(i);
            if(transaction != null && transaction.category != null && !transaction.category.trim().equals("") && !categories.contains(transaction.category))
                categories.add(transaction.category);
        }
        return total;
    }
}
