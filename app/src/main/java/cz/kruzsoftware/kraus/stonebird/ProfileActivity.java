package cz.kruzsoftware.kraus.stonebird;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    ProfileActivity context = this;

    FirebaseUser user;

    FirebaseDatabase db = FirebaseDatabase.getInstance();
    DatabaseReference profilesRef;

    SharedPreferences defSharedPref;
    String currentProfileSharedPrefKey;

    HashMap<String, Profile> profileHashMap;

    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private FloatingActionButton fab;

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            profileHashMap = new HashMap<>();
            for (DataSnapshot child : dataSnapshot.getChildren()) {
                profileHashMap.put(child.getKey(), child.getValue(Profile.class));
            }

            updateUi();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private final String TAG = "ProfileActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        user = FirebaseAuth.getInstance().getCurrentUser();

        progressBar = findViewById(R.id.progressBar_profileActivity);
        linearLayout = findViewById(R.id.linearLayout);
        fab = findViewById(R.id.fab_add);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int max = 0;
                for(Object o : profileHashMap.entrySet()) {
                    HashMap.Entry entry = (HashMap.Entry) o;
                    int id = Integer.parseInt((String) entry.getKey());
                    max = Math.max(max, id);
                }
                Intent intent = new Intent(context, EditProfileActivity.class);
                intent.putExtra("profileId", Integer.toString(max + 1));

                startActivity(intent);
            }
        });

        defSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        currentProfileSharedPrefKey = getString(R.string.shared_pref_profile_id) + user.getUid();

        profilesRef = db.getReference("profiles").child(user.getUid()).orderByChild("name").getRef();
        profilesRef.addValueEventListener(valueEventListener);
    }

    private void updateUi() {
        if (profileHashMap == null) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);

            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                linearLayout.removeAllViews();

                for (Object o : profileHashMap.entrySet()) {
                    final HashMap.Entry entry = (HashMap.Entry) o;
                    final Profile iteratorProfile = (Profile) entry.getValue();

                    @SuppressLint("InflateParams") View view = layoutInflater.inflate(R.layout.profile_layout, null);
                    view.setTag(entry.getKey());

                    final Button selectButton = view.findViewById(R.id.button_selectProfile);
                    selectButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            View otherProfileCardView = linearLayout.findViewWithTag(defSharedPref.getString(currentProfileSharedPrefKey, ""));
                            if(otherProfileCardView != null) {
                                Button otherButton = otherProfileCardView.findViewById(R.id.button_selectProfile);
                                otherButton.setEnabled(true);
                            }
                            defSharedPref.edit().putString(currentProfileSharedPrefKey, (String) entry.getKey()).apply();
                            selectButton.setEnabled(false);
                        }
                    });
                    if (defSharedPref.getString(currentProfileSharedPrefKey, "null").equals(entry.getKey())) {
                        selectButton.setEnabled(false);
                    }

                    TextView profileNameTextView = view.findViewById(R.id.textView_profileName);
                    profileNameTextView.setText(iteratorProfile.name);

                    NumberFormat numberFormat = NumberFormat.getInstance(Locale.ENGLISH);
                    numberFormat.setMaximumFractionDigits(2);
                    TextView moneyTextView = view.findViewById(R.id.textView_money);
                    String money = numberFormat.format(iteratorProfile.value()) + " " + iteratorProfile.currency.toUpperCase();
                    moneyTextView.setText(money);
                    if(iteratorProfile.value() < 0) moneyTextView.setTextColor(getResources().getColor(android.R.color.holo_red_dark));

                    Button editButton = view.findViewById(R.id.button_edit);
                    editButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, EditProfileActivity.class);
                            intent.putExtra("profileId", (String) entry.getKey());
                            startActivity(intent);
                        }
                    });

                    Button deleteButton = view.findViewById(R.id.button_delete);
                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new AlertDialog.Builder(context)
                                    .setMessage(getString(R.string.delete_profile_message) + " " + iteratorProfile.name + "?")
                                    .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            progressBar.setVisibility(View.VISIBLE);
                                            linearLayout.setVisibility(View.GONE);
                                            profilesRef.child((String) entry.getKey()).removeValue(new DatabaseReference.CompletionListener() {
                                                @Override
                                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                                    if(defSharedPref.getString(getString(R.string.shared_pref_profile_id) + user.getUid(), "")
                                                            .equals(entry.getKey())) {
                                                        defSharedPref.edit().remove(getString(R.string.shared_pref_profile_id) + user.getUid()).apply();
                                                    }
                                                }
                                            });
                                        }
                                    }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                        }
                    });

                    linearLayout.addView(view);


                    /*CardView cardView = new CardView(this);
                    cardView.setTag(entry.getKey());
                    LinearLayout.LayoutParams cardViewLayoutParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    cardViewLayoutParams.setMargins(0, dpToPx(4), 0, dpToPx(4));
                    cardView.setLayoutParams(cardViewLayoutParams);
                    linearLayout.addView(cardView);

                    ConstraintLayout constraintLayout = new ConstraintLayout(this);
                    LinearLayout.LayoutParams constraintLayoutLayoutParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    );
                    constraintLayout.setLayoutParams(constraintLayoutLayoutParams);
                    cardView.addView(constraintLayout);

                    @SuppressLint("InflateParams") final Button selectButton = (Button) getLayoutInflater().inflate(R.layout.button_borderless_colored, null);
                    selectButton.setId(R.id.button_selectProfile);
                    selectButton.setText(R.string.select);
                    ConstraintLayout.LayoutParams selectLayoutParams = new ConstraintLayout.LayoutParams(
                            ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT
                    );
                    selectButton.setLayoutParams(selectLayoutParams);
                    constraintLayout.addView(selectButton);

                    selectButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            View otherProfileCardView = linearLayout.findViewWithTag(defSharedPref.getString(currentProfileSharedPrefKey, ""));
                            if(otherProfileCardView != null) {
                                Button otherButton = otherProfileCardView.findViewById(R.id.button_selectProfile);
                                otherButton.setEnabled(true);
                            }
                            defSharedPref.edit().putString(currentProfileSharedPrefKey, (String) entry.getKey()).apply();
                            selectButton.setEnabled(false);
                        }
                    });
                    if (defSharedPref.getString(currentProfileSharedPrefKey, "null").equals(entry.getKey())) {
                        selectButton.setEnabled(false);
                    }

                    @SuppressLint("InflateParams") TextView textViewProfileName = (TextView) getLayoutInflater().inflate(R.layout.text_small, null);
                    textViewProfileName.setId(R.id.textView_profileName);
                    textViewProfileName.setText(iteratorProfile.name);
                    textViewProfileName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                    ConstraintLayout.LayoutParams profileNameLayoutParams = new ConstraintLayout.LayoutParams(
                            ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, ConstraintLayout.LayoutParams.WRAP_CONTENT
                    );
                    textViewProfileName.setLayoutParams(profileNameLayoutParams);
                    constraintLayout.addView(textViewProfileName);

                    @SuppressLint("InflateParams") TextView textViewMoney = (TextView) getLayoutInflater().inflate(R.layout.text_small, null);
                    textViewMoney.setId(R.id.textView_money);
                    textViewMoney.setText("420 USD");
                    textViewMoney.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                    ConstraintLayout.LayoutParams cashLayoutParams = new ConstraintLayout.LayoutParams(
                            ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, ConstraintLayout.LayoutParams.WRAP_CONTENT
                    );
                    textViewMoney.setLayoutParams(cashLayoutParams);
                    constraintLayout.addView(textViewMoney);

                    @SuppressLint("InflateParams") Button editButton = (Button) getLayoutInflater().inflate(R.layout.button_borderless_colored, null);
                    editButton.setId(R.id.button_edit);
                    editButton.setText(R.string.edit);
                    ConstraintLayout.LayoutParams editLayoutParams = new ConstraintLayout.LayoutParams(
                            ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT
                    );
                    editButton.setLayoutParams(editLayoutParams);
                    constraintLayout.addView(editButton);

                    editButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, EditProfileActivity.class);
                            intent.putExtra("profileId", (String) entry.getKey());
                            startActivity(intent);
                        }
                    });

                    @SuppressLint("InflateParams") Button deleteButton = (Button) getLayoutInflater().inflate(R.layout.button_borderless_colored, null);
                    deleteButton.setId(R.id.button_delete);
                    deleteButton.setText(R.string.delete);
                    ConstraintLayout.LayoutParams deleteLayoutParams = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    deleteButton.setLayoutParams(deleteLayoutParams);
                    constraintLayout.addView(deleteButton);

                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new AlertDialog.Builder(context)
                                    .setMessage(getString(R.string.delete_profile_message) + " " + iteratorProfile.name + "?")
                                    .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            progressBar.setVisibility(View.VISIBLE);
                                            linearLayout.setVisibility(View.GONE);
                                            profilesRef.child((String) entry.getKey()).removeValue(new DatabaseReference.CompletionListener() {
                                                @Override
                                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                                    if(defSharedPref.getString(getString(R.string.shared_pref_profile_id) + user.getUid(), "")
                                                            .equals(entry.getKey())) {
                                                        defSharedPref.edit().remove(getString(R.string.shared_pref_profile_id) + user.getUid()).apply();
                                                    }
                                                }
                                            });
                                        }
                                    }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    }).show();
                        }
                    });

                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(constraintLayout);
                    constraintSet.connect(R.id.button_selectProfile, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0);
                    constraintSet.connect(R.id.button_selectProfile, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, 0);
                    constraintSet.connect(R.id.textView_profileName, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, dpToPx(8));
                    constraintSet.connect(R.id.textView_profileName, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT, dpToPx(16));
                    constraintSet.connect(R.id.textView_profileName, ConstraintSet.RIGHT, R.id.button_selectProfile, ConstraintSet.LEFT, 0);
                    constraintSet.connect(R.id.textView_money, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT, dpToPx(16));
                    constraintSet.connect(R.id.textView_money, ConstraintSet.TOP, R.id.textView_profileName, ConstraintSet.BOTTOM, dpToPx(16));
                    constraintSet.connect(R.id.textView_money, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, dpToPx(16));
                    constraintSet.connect(R.id.button_edit, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, 0);
                    constraintSet.connect(R.id.button_edit, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0);
                    constraintSet.connect(R.id.button_edit, ConstraintSet.TOP, R.id.textView_money, ConstraintSet.BOTTOM, 0);
                    constraintSet.connect(R.id.button_delete, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0);
                    constraintSet.connect(R.id.button_delete, ConstraintSet.RIGHT, R.id.button_edit, ConstraintSet.LEFT, 0);
                    constraintSet.connect(R.id.button_delete, ConstraintSet.TOP, R.id.textView_money, ConstraintSet.BOTTOM, 0);

                    constraintLayout.setConstraintSet(constraintSet);*/
                }
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
