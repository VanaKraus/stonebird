package cz.kruzsoftware.kraus.stonebird;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;

public class EditProfileActivity extends AppCompatActivity {

    FirebaseUser user;

    String profileId;
    Profile profile;

    ConstraintLayout container;
    EditText nameEditText;
    FloatingActionButton fab;
    boolean fabEnabled = true;
    ProgressBar progressBar;
    ProgressBar mainProgressBar;
    ScrollView scrollView;
    Button currencyButton;

    DatabaseReference databaseReference;

    ArrayList<String> currencies = new ArrayList<>();

    EditProfileActivity context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        user = FirebaseAuth.getInstance().getCurrentUser();

        container = findViewById(R.id.constraintLayout);
        nameEditText = findViewById(R.id.editText_name);
        fab = findViewById(R.id.fab_editProfile);
        progressBar = findViewById(R.id.progressBar_editProfile);
        mainProgressBar = findViewById(R.id.progressBar_main);
        scrollView = findViewById(R.id.scrollView);
        currencyButton = findViewById(R.id.button_currency);

        Intent intent = getIntent();
        profileId = intent.getStringExtra("profileId");
        databaseReference = FirebaseDatabase.getInstance().getReference("profiles").child(user.getUid()).child(profileId);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    profile = dataSnapshot.getValue(Profile.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(profile == null) {
                    profile = new Profile("New Profile", "");
                }
                if(profile.transactions == null) {
                    profile.transactions = new ArrayList<>();
                }
                if(profile.transactions.size() == 0) {
                    profile.transactions.add(new Transaction(1, new Date(), "d", "d"));
                }

                setUi();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                finish();
            }
        });

        FirebaseDatabase.getInstance().getReference("currencies").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot child : dataSnapshot.getChildren()) {
                    currencies.add(child.getValue(String.class));
                }
                setUi();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                finish();
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fabEnabled && profile != null) {
                    profile.name = nameEditText.getText().toString();
                    profile.currency = currencyButton.getText().toString();

                    if(profile.name.length() == 0) {
                        Snackbar.make(container, R.string.missing_profile_name, Snackbar.LENGTH_LONG).show();
                        return;
                    }

                    fabEnabled = false;
                    progressBar.setVisibility(View.VISIBLE);

                    databaseReference.setValue(profile, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            if(databaseError != null) {
                                progressBar.setVisibility(View.INVISIBLE);
                                fabEnabled = true;
                            } else {
                                finish();
                            }
                        }
                    });
                }
            }
        });

        setUi();
    }

    void setUi() {
        if(profile == null || currencies.size() == 0) {
            fab.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            mainProgressBar.setVisibility(View.VISIBLE);
            return;
        }

        nameEditText.setText(profile.name);
        if(profile.currency == null || profile.currency.equals("")) {
            profile.currency = currencies.get(0);
        }

        if(profile.transactions != null && profile.transactions.size() > 1) {
            currencyButton.setEnabled(false);
        }
        currencyButton.setText(profile.currency);
        currencyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] array = currencies.toArray(new String[0]);
                for(int i = 0; i < array.length; i++) {
                    array[i] = array[i].toUpperCase();
                }
                new AlertDialog.Builder(context)
                        .setItems(array, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                currencyButton.setText(currencies.get(which));
                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .show();

            }
        });

        mainProgressBar.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        fab.setVisibility(View.VISIBLE);
    }
}
