package cz.kruzsoftware.kraus.stonebird;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

class Transaction {
    public static SimpleDateFormat universalSdf;

    public double amount;
    public String date;
    public String label;
    public String category;

    @SuppressLint("SimpleDateFormat")
    public Transaction() {
        // Because of the realtime database

        if(universalSdf == null) {
            universalSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            universalSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
    }

    @SuppressLint("SimpleDateFormat")
    Transaction(double amount, Date dateObj, String label, String category) {
        if(universalSdf == null) {
            universalSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            universalSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        this.amount = amount;
        this.label = label;
        this.category = category;

        date = universalSdf.format(dateObj);
    }

    public static Date parseDate(String date) {
        try {
            return universalSdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
