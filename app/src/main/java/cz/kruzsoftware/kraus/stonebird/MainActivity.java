package cz.kruzsoftware.kraus.stonebird;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 123;
    FirebaseUser user;

    private TextView statusTextView;
    private ConstraintLayout container;
    private Button budgetManagerButton;
    private Button signOutButton, signInButton;

    private MainActivity context = this;

    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statusTextView = findViewById(R.id.textView_status);
        container = findViewById(R.id.container);
        budgetManagerButton = findViewById(R.id.button_budgetManager);
        budgetManagerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BudgetManagerActivity.class);
                startActivity(intent);
            }
        });

        signOutButton = findViewById(R.id.button_signOut);
        signOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });

        signInButton = findViewById(R.id.button_signIn);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateUI();
        if(user == null) {
            signIn();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            updateUI();

            if (resultCode != RESULT_OK) {
                if(response != null) {
                    Snackbar.make(container, "An error occured while signing in", Snackbar.LENGTH_LONG).show();
                    Log.d(TAG, Objects.requireNonNull(response.getError()).getMessage());
                }
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }

    private void signIn() {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                        RC_SIGN_IN);
    }

    private void signOut() {
        AuthUI.getInstance().signOut(this).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                updateUI();
            }
        });
    }

    private void updateUI() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user == null) {
            signInButton.setText(R.string.sign_in);
            statusTextView.setVisibility(View.GONE);
            signOutButton.setVisibility(View.GONE);
            budgetManagerButton.setVisibility(View.GONE);
        } else {
            signInButton.setText(R.string.change_account);
            statusTextView.setText(user.getDisplayName());
            statusTextView.setVisibility(View.VISIBLE);
            signOutButton.setVisibility(View.VISIBLE);
            budgetManagerButton.setVisibility(View.VISIBLE);
        }
    }
}
